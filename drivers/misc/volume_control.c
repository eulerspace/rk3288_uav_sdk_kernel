/*
 * An I2C driver for the volume_control
 *
 * based on the other drivers in this same directory.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/i2c.h>
#include <linux/bcd.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/fb.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>
#include <linux/err.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <linux/platform_device.h>
#define DRV_VERSION "0.4.3"
#define I2CDATANUM 14
typedef struct
{
    char reg;
    char data[I2CDATANUM];
    uint8_t SlaveAddress;
    int cmd;
    int count;
    uint32_t scl_rate;
}Data6050;
//static struct i2c_driver mpu6050_driver;
static struct i2c_client *mpu6050_client = NULL;


static const struct of_device_id mpu6050_of_match[] = {
	{ .compatible = "mpu6050" },
	{ }
};
MODULE_DEVICE_TABLE(of, mpu6050_of_match);


//i2c发送函数 
static int i2c_master_reg8_send(const struct i2c_client *client,const Data6050 *sendData)
{
    struct i2c_adapter *adap=client->adapter;
    struct i2c_msg msg;
    int ret;
    char *tx_buf = (char *)kzalloc(sendData->count + 1, GFP_KERNEL);
    if(!tx_buf)
        return -ENOMEM;
    tx_buf[0] = sendData->reg;
    memcpy(tx_buf+1, sendData->data, sendData->count); 

    msg.addr = sendData->SlaveAddress;
    msg.flags = client->flags;
    msg.len = sendData->count + 1;
    msg.buf = (char *)tx_buf;
    msg.scl_rate = sendData->scl_rate;
 
    ret = i2c_transfer(client->adapter, &msg, 1);
    
    kfree(tx_buf);
    return (ret == 1) ? sendData->count : ret;
 
}
//i2c接收函数
static int i2c_master_reg8_recv(const struct i2c_client *client,Data6050 *recvData)
{
    struct i2c_adapter *adap=client->adapter;
    struct i2c_msg msgs[2];
    int ret;
    char reg_buf = recvData->reg;
 	
    msgs[0].addr = recvData->SlaveAddress;
    msgs[0].flags = client->flags;

    msgs[0].len = 1;
    msgs[0].buf = &reg_buf;
    msgs[0].scl_rate = recvData->scl_rate;
 
    msgs[1].addr = recvData->SlaveAddress;
    msgs[1].flags = client->flags | I2C_M_RD;
    msgs[1].len = recvData->count;
    msgs[1].buf = recvData->data;
    msgs[1].scl_rate = recvData->scl_rate;
 
    ret = i2c_transfer(adap, msgs, 2); 
 
    return (ret == 2)? recvData->count : ret;
}
  
static int mpu6050_open(struct inode *inode, struct file *file)  
{  
    printk("mpu6050_open\n");
    return 0;
}  

static int mpu6050_read(struct file *filp, char __user *ptr, size_t size, loff_t *pos)  
{  
    if (ptr == NULL)
		printk("%s: user space address is NULL\n", __func__);
	return sizeof(int);
}

static int mpu6050_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)  
{  
    int ret = 0;
	Data6050 recvData;
    memset(&recvData,0,sizeof(recvData));
    // 
    if(copy_from_user(&recvData,(Data6050 *)arg,sizeof(Data6050)))
    {
        return -EFAULT;
    }
	// options : 0 write data  1 read data
	switch (recvData.cmd)
	{
        case 0:
            ret = i2c_master_reg8_send(mpu6050_client,&recvData);//
            break;
        case 1:

		    ret = i2c_master_reg8_recv(mpu6050_client,&recvData);
		    if(ret > 0)
		    {
		        if(copy_to_user(((Data6050 *)arg)->data,recvData.data,I2CDATANUM))
		        {
		            return -1;
		        }
		    }
            break;
			
	    default:
			    printk("unknown ioctl cmd!\n");
			    ret = -1;
			    break;
	}
	
	return ret;
}
static int mpu6050_release(struct inode *inode, struct file *filp)
{
    printk("mpu6050_release\n");
    
	return 0;
}
static struct file_operations mpu6050_fops = {  
    .owner   = THIS_MODULE,  
    .open    = mpu6050_open,  
    .read    = mpu6050_read,      
    .unlocked_ioctl = mpu6050_ioctl,
    .release = mpu6050_release, 
};

static struct miscdevice mpu6050_dev = 
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = "mpu6050",
    .fops = &mpu6050_fops,
};



static int mpu6050_probe(struct i2c_client *client,const struct i2c_device_id *id)
{
	//struct proc_dri_entry *file;
    int ret=-1;
	//dev_dbg(&client->dev, "%s\n", __func__);

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
		return -ENODEV;

	ret = misc_register(&mpu6050_dev);
	//dev_info(&client->dev, "chip found, driver version " DRV_VERSION "\n");
    if (ret < 0)
    {
		printk("mpu6050 misc register err!\n");
		return ret;
	}
	
	mpu6050_client = client;	
	printk("mpu6050_probe\n");
	
	return 0;
}

static int mpu6050_remove(struct i2c_client *client)
{
	//remove_proc_entry("volume_proc", NULL); 
	return 0;
}
#ifdef CONFIG_PM_SLEEP
static int mpu6050_suspend(struct i2c_client *client)
{
        //printk("func: %s\n", __func__); 
	return 0;
}

static int mpu6050_resume(struct i2c_client *client)
{
        //printk("func: %s\n", __func__); 
	return 0;
}
#endif

static const struct i2c_device_id mpu6050_id[] = {
	{ "mpu6050", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, mpu6050_id);



static struct i2c_driver mpu6050_driver = {
	.driver		= {
		.name	= "mpu6050",
		//.owner	= THIS_MODULE,
		//.of_match_table = mpu6050_of_match,
	},
	.probe		= mpu6050_probe,
	.remove		= mpu6050_remove,
	.id_table	= mpu6050_id,
};

//i2c_add_driver(&mpu6050_driver);
//module_i2c_driver(mpu6050_driver);

static int __init mpu6050_init(void)
{
	return i2c_add_driver(&mpu6050_driver);
}
module_init(mpu6050_init);

static void __exit mpu6050_exit(void)
{
	i2c_del_driver(&mpu6050_driver);
}
MODULE_DESCRIPTION("mpu6050 test");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:mpu6050");
