#include <dt-bindings/gpio/gpio.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/fb.h>
#include <linux/backlight.h>
#include <linux/err.h>
#include <linux/pwm.h>
#include <linux/pwm_backlight.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <asm/uaccess.h>

#define CS_HMC5983 237

int gpio,flag;
int gpio2,flag2;



 static struct UserData{
		int gpio;
		int cmd;
		int state;
};


static struct of_device_id luobogpio_of_match[] = {
	{ .compatible = "luobogpio" },
	{ }
};

MODULE_DEVICE_TABLE(of,luobogpio_of_match);


static int luobogpio_open(struct inode *inode, struct file *filp)
{
    int ret = -1;
    printk("luobogpio_open gpio = %d\n",gpio);
    printk("luobogpio_open gpio2 = %d\n",gpio2);
	if(!gpio_is_valid(gpio))
    {
    	printk("invalid iopin %d\n",gpio);
    	return ret;
    }
    //gpio_direction_output(gpio,1);
	return 0;
}

static ssize_t luobogpio_read(struct file *filp, char __user *ptr, size_t size, loff_t *pos)
{
	if (ptr == NULL)
		printk("%s: user space address is NULL\n", __func__);
	return sizeof(int);
}

static long luobogpio_ioctl(struct file *filp, int cmd, unsigned long arg)
{
	long ret = 0;	
	struct UserData userdata;
	unsigned char label[10];
	if (copy_from_user((void*)&userdata,(void __user *)arg, sizeof(struct UserData))) 
		        return -EFAULT;
	//printk("androidUAV : %s cmd %d\n",__func__,cmd);
	switch (userdata.cmd)
	{
		case 0:
		        sprintf(label,"gpio-%d",userdata.gpio);
				ret = gpio_request(userdata.gpio,label);
				if (ret)
				{
		       		printk("failed to request GPIO%d for you ret:%d\n",userdata.gpio,ret);
				}
			  break;
		case 1://set gpio as output mode
				ret=gpio_direction_output(userdata.gpio, userdata.state);
			  	if (ret) 
			  	{
					printk("failed to gpio_direction_output  for you ret:%d\n",userdata.gpio);
				}
		  break;
		case 2://set gpio status after io is output mode
				gpio_set_value(userdata.gpio, userdata.state);
			  	if (ret) 
			  	{
					printk("failed to gpio_set_value  for you ret:%d\n",userdata.gpio);
				}
		  break;
		case 3://set gpio as input mode
				ret=gpio_direction_input(userdata.gpio);
			  	if (ret) 
			  	{
					printk("failed to gpio_direction_input  for you ret:%d\n",userdata.gpio);
				}
		  break;
		case 4://read gpio status
			ret=gpio_get_value(userdata.gpio);
		  	
		break;
		case 5:
				gpio_free(userdata.gpio);

		  break;
		
	  	default:
				printk("unknown ioctl cmd!\n");
				ret = -EINVAL;
				break;
	}
	return ret;
}

static int luobogpio_release(struct inode *inode, struct file *filp)
{
    printk("luobogpio_release\n");
    
	return 0;
}

static struct file_operations luobogpio_fops = {
	.owner   = THIS_MODULE,
	.open    = luobogpio_open,
	.read    = luobogpio_read,
	.unlocked_ioctl   = luobogpio_ioctl,
	.release = luobogpio_release,
};

static struct miscdevice luobogpio_dev = 
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = "luobogpio",
    .fops = &luobogpio_fops,
};


static int luobogpio_probe(struct platform_device *pdev)
{
    int ret=-1;
    struct device_node *io_node = pdev->dev.of_node;
	ret = misc_register(&luobogpio_dev);
	if (ret < 0){
		printk("ac_usb_switch register err!\n");
		return ret;
	}
	
    printk("func: %s\n", __func__); 
    
    ret=-1;
    
    
    gpio = of_get_named_gpio_flags(io_node,"iopin",0,&flag);
    gpio2 = of_get_named_gpio_flags(io_node,"rstpin",0,&flag2);
    //D/C ctl pin gpio7 A4
    if(!gpio_is_valid(gpio))
    {
    	printk("invalid iopin %d\n",gpio);
    	return ret;
    }
    if(gpio_request(gpio,"iopin"))
    {
    	printk("gpio %d request failed!\n",gpio);
    	return ret;
    }
    printk("cuccess init gpio = %d value = %d high = %d\n",gpio,flag,OF_GPIO_ACTIVE_LOW);
    //rest pin gpio7 A6
    if(!gpio_is_valid(gpio2))
    {
    	printk("invalid iopin %d\n",gpio2);
    	return ret;
    }
    if(gpio_request(gpio2,"rstpin"))
    {
    	printk("gpio %d request failed!\n",gpio2);
    	return ret;
    }
    
    if(gpio_request(CS_HMC5983,"cs_hmc5983"))
    {
    	printk("cs_hmc5983 gpio %d request failed!\n",CS_HMC5983);
    }
    printk("cuccess init gpio = %d value = %d high = %d\n",gpio2,flag2,OF_GPIO_ACTIVE_LOW);
    
    return 0;
}

static int luobogpio_remove(struct platform_device *pdev)
{
        //printk("func: %s\n", __func__); 
	return 0;
}

#ifdef CONFIG_PM_SLEEP
static int luobogpio_suspend(struct device *dev)
{
        //printk("func: %s\n", __func__); 
	return 0;
}

static int luobogpio_resume(struct device *dev)
{
        //printk("func: %s\n", __func__); 
	return 0;
}
#endif

static const struct dev_pm_ops luobogpio_pm_ops = {
#ifdef CONFIG_PM_SLEEP
	.suspend = luobogpio_suspend,
	.resume = luobogpio_resume,
	.poweroff = luobogpio_suspend,
	.restore = luobogpio_resume,
#endif
};

static struct platform_driver luogpio_driver = {
	.driver		= {
		.name		= "luobogpio",
		.owner		= THIS_MODULE,
		.pm		= &luobogpio_pm_ops,
		.of_match_table	= of_match_ptr(luobogpio_of_match),
	},
	.probe		= luobogpio_probe,
	.remove		= luobogpio_remove,
};

module_platform_driver(luogpio_driver);

MODULE_DESCRIPTION("luobogpio Driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:luobogpio");
