#include <linux/err.h>
#include <linux/platform_device.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/platform_device.h>
#include <linux/fb.h>
#include <linux/backlight.h>
#include <linux/err.h>
#include <linux/pwm.h>
#include <linux/pwm_backlight.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/miscdevice.h>
#include <asm/uaccess.h>

#define PWMNUM 4
#define LABELSIZE 20
#define REGISALREADY 15
typedef struct pwm_dat
{
	int pwm_id;
	int duty_ns;
	int period_ns;
}pwm_data;
struct pwm_device *eulerpwm0 = NULL;
struct pwm_device *eulerpwms[PWMNUM] = {NULL,NULL,NULL,NULL};
static int pwmnumNow = 0;
char label[LABELSIZE];
static struct of_device_id eulerGPIO_of_match[] ={ 
	{ .compatible = "eulerPWM0" },
	{},
};
MODULE_DEVICE_TABLE(of, eulerGPIO_of_match);


static int eulerpwm0_open(struct inode *inode, struct file *filp)
{
    printk("eulerpwm0_open\n");
	return 0;
}

static ssize_t eulerpwm0_read(struct file *filp, char __user *ptr, size_t size, loff_t *pos)
{
	if (ptr == NULL)
		printk("%s: user space address is NULL\n", __func__);
	return sizeof(int);
}

static long eulerpwm0_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int ret = 0;
	pwm_data userconfig = {-1,0,0};
	int i;
	if(!(void __user *)arg)
		return -EFAULT;
	if (copy_from_user((void*)&userconfig,(void __user *)arg, sizeof(pwm_data))) 
		        return -EFAULT;
    if(userconfig.pwm_id < 0)
    	return -EFAULT;
	switch (cmd)
	{
		//config pwm parameter if cmd = 0
		case 0:
			ret = pwm_config(eulerpwms[userconfig.pwm_id],userconfig.duty_ns,userconfig.period_ns);
			if(ret < 0)
				printk("pwm-%d config error\n",userconfig.pwm_id);
			break;
		//enable pwm
		case 1:
			ret = pwm_enable(eulerpwms[userconfig.pwm_id]);
			if(ret < 0)
				printk("pwm-%d enable error\n",userconfig.pwm_id);
			break;
		//disable pwm
		case 2:
			pwm_disable(eulerpwms[userconfig.pwm_id]);
		//request pwm here
		case 3:
			if(eulerpwms[userconfig.pwm_id])
			{
				return -REGISALREADY;
			}
			
			sprintf(label,"pwm%d",userconfig.pwm_id);
			
			eulerpwms[userconfig.pwm_id] = pwm_request(userconfig.pwm_id,label);
			if(eulerpwms[userconfig.pwm_id] == NULL)
			{
				printk("error reuqest\n");
				return -1;
			}
			for(i=0;i<4;i++)
			{
				printk("pwm [%d]%x\n",i,eulerpwms[i]);
			}
			pwmnumNow++;
			printk("owm driver have resister %s pwmnum = %d\n",label,pwmnumNow);
			return 0;
			break;
	}
	return ret;
}

static int eulerpwm0_release(struct inode *inode, struct file *filp)
{
    int i;
    printk("eulerpwm0_release\n");
    for(i = 0;i<PWMNUM;i++)
    {
    	if(eulerpwms[i])
    		pwm_disable(eulerpwms[i]);
    }
	return 0;
}



#ifdef CONFIG_PM_SLEEP
static int eulerpwm0_suspend(struct device *dev)
{
        //printk("func: %s\n", __func__); 
	return 0;
}

static int eulerpwm0_resume(struct device *dev)
{
        //printk("func: %s\n", __func__); 
	return 0;
}
#endif

static struct file_operations eulerpwm0_fops = {
	.owner   = THIS_MODULE,
	.open    = eulerpwm0_open,
	.read    = eulerpwm0_read,
	.unlocked_ioctl   = eulerpwm0_ioctl,
	.release = eulerpwm0_release,
};
static struct miscdevice eulerpwm0_dev = 
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = "eulerpwm0",
    .fops = &eulerpwm0_fops,
};
static int eulerPWM0_probe(struct platform_device *pdev)
{
	int ret;
	int i;
	int errCnt = 0;
	struct device_node *io_node = pdev->dev.of_node;
	unsigned char label[LABELSIZE];
	ret = misc_register(&eulerpwm0_dev);
	if (ret < 0){
		printk("ac_usb_switch register err!\n");
		return ret;
	}
	/*for(i=0;i<PWMNUM;i++)
	{
		sprintf(label,"pwm%d",i);
		eulerpwms[i] = pwm_request(i,label);
		if(eulerpwms[i] == NULL)
		{
			printk("error register %s",label);
			errCnt++;
		}		
	}
	pwmnumNow = PWMNUM-errCnt;*/
	printk("pwm probe end,success register pwm device number:%d\n",pwmnumNow);
    return 0;
}
static int eulerPWM0_remove(struct platform_device *pdev)
{
        //printk("func: %s\n", __func__); 
	return 0;
}

static const struct dev_pm_ops eulerPWM0_pm_ops = {
#ifdef CONFIG_PM_SLEEP
	.suspend = eulerpwm0_suspend,
	.resume = eulerpwm0_resume,
	.poweroff = eulerpwm0_suspend,
	.restore = eulerpwm0_resume,
#endif
};
static struct platform_driver eulerPWM0_driver = {
	.driver		= {
		.name		= "eulerPWM0",
		.owner		= THIS_MODULE,
		.pm		= &eulerPWM0_pm_ops,
		.of_match_table	= of_match_ptr(eulerGPIO_of_match),
	},
	.probe		= eulerPWM0_probe,
	.remove		= eulerPWM0_remove,
};
module_platform_driver(eulerPWM0_driver);

MODULE_AUTHOR("Euler Yang <yangzegang@eulerspace.com>");
MODULE_DESCRIPTION("GPIO Pulse Width Modulation Driver");
MODULE_ALIAS("platform:rk3288 UAV pwm");
MODULE_LICENSE("GPL v2");
