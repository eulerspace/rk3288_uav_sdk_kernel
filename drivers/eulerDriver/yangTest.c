/*
 * An I2C driver for the volume_control
 *
 * based on the other drivers in this same directory.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include <linux/i2c.h>
#include <linux/bcd.h>
#include <linux/slab.h>
#include <linux/module.h>
#include <linux/of.h>
#include <linux/delay.h>
#include <linux/proc_fs.h>
#include <linux/fb.h>
#include <linux/kernel.h>
#include <asm/uaccess.h>
#include <linux/err.h>

#define DRV_VERSION "0.4.3"

static char *str = NULL;  
static struct i2c_driver volume_driver;
static struct i2c_client *volume_client;

//i2c发送函数 *client 设备  reg 寄存器  *buf 数据 count  数据大小  scl_rate i2c速率
static int i2c_master_reg8_send(const struct i2c_client *client, const char reg, const char *buf, int count, int scl_rate)
{
    struct i2c_adapter *adap=client->adapter;
    struct i2c_msg msg;
    int ret;
    char *tx_buf = (char *)kzalloc(count + 1, GFP_KERNEL);
    if(!tx_buf)
        return -ENOMEM;
    tx_buf[0] = reg;
    memcpy(tx_buf+1, buf, count); 
 
    msg.addr = client->addr;
    msg.flags = client->flags;
    msg.len = count + 1;
    msg.buf = (char *)tx_buf;
    msg.scl_rate = scl_rate;
 
    ret = i2c_transfer(adap, &msg, 1); 
    kfree(tx_buf);
    return (ret == 1) ? count : ret;
 
}
//i2c接收函数
static int i2c_master_reg8_recv(const struct i2c_client *client, const char reg, char *buf, int count, int scl_rate)
{
    struct i2c_adapter *adap=client->adapter;
    struct i2c_msg msgs[2];
    int ret;
    char reg_buf = reg;
 
    msgs[0].addr = client->addr;
    msgs[0].flags = client->flags;
    msgs[0].len = 1;
    msgs[0].buf = &reg_buf;
    msgs[0].scl_rate = scl_rate;
 
    msgs[1].addr = client->addr;
    msgs[1].flags = client->flags | I2C_M_RD;
    msgs[1].len = count;
    msgs[1].buf = (char *)buf;
    msgs[1].scl_rate = scl_rate;
 
    ret = i2c_transfer(adap, msgs, 2); 
 
    return (ret == 2)? count : ret;
}
 

/*实现open和write函数*/  
static ssize_t volume_proc_write(struct file *file, const char __user *buffer,  
                             size_t count, loff_t *f_pos)  
{  
    char *tmp = kzalloc((count+1), GFP_KERNEL);  
    int channel;
    if(!tmp)  
        return -ENOMEM;  
    if(copy_from_user(tmp, buffer, count))  
    {  
        kfree(tmp);  
        return EFAULT;  
    }  
    kfree(str);  
    str = tmp;  
    
    sscanf(tmp,"%d",&channel);
    if(channel< 0 || channel >254)
    {
    //	printk("the date is:%d\n",channel);
	printk("the date is over\n");
	return count;
    }

    i2c_master_reg8_send(volume_client, 0xD9, &channel,sizeof(channel), 20000);//发送channel到寄存器0xd9

    return count;  
}  
  
static int volume_proc_show(struct seq_file *m, void *v)  
{  
    seq_printf(m, "last date is %s\n", str);  
    return 0;  
}
  
static int volume_proc_open(struct inode *inode, struct file *file)  
{  
    return single_open(file, volume_proc_show, NULL);  
}  
  
static struct file_operations volume_fops = {  
    .owner   = THIS_MODULE,  
    .open    = volume_proc_open,  
    .release = single_release,  
    .read    = seq_read,  
    .llseek  = seq_lseek,  
    .write   = volume_proc_write,  
};  

static int volume_probe(struct i2c_client *client,
				const struct i2c_device_id *id)
{
	struct proc_dri_entry *file; 
	dev_dbg(&client->dev, "%s\n", __func__);

	if (!i2c_check_functionality(client->adapter, I2C_FUNC_I2C))
		return -ENODEV;

	dev_info(&client->dev, "chip found, driver version " DRV_VERSION "\n");

	file = proc_create("volume_proc", 0666, NULL, &volume_fops); // 节点路径:  /proc/volume_proc
    	if(!file)  
        	return -ENOMEM;
	volume_client = client;	
	printk("volume_probe\n");
	
	return 0;
}

static int volume_remove(struct i2c_client *client)
{
	remove_proc_entry("volume_proc", NULL); 
	return 0;
}


static const struct i2c_device_id volume_id[] = {
	{ "mpu6050", 0 },
	{ }
};
MODULE_DEVICE_TABLE(i2c, volume_id);

static const struct of_device_id volume_of_match[] = {
	{ .compatible = "invensense,mpu6050" },
	{}
};
MODULE_DEVICE_TABLE(of, volume_of_match);

static struct i2c_driver volume_driver = {
	.driver		= {
		.name	= "mpu6050",
		.owner	= THIS_MODULE,
		.of_match_table = volume_of_match,
	},
	.probe		= volume_probe,
	.remove		= volume_remove,
	.id_table	= volume_id,
};

module_i2c_driver(volume_driver);

i2c_add_driver(&volume_driver);
MODULE_AUTHOR("euler-Yang");
MODULE_DESCRIPTION("mpu6050 test");
MODULE_LICENSE("GPL");
MODULE_VERSION(DRV_VERSION);
