/**
*	This is a simple demo for io driver
*	last modified by
*	12-15-2016 Yang - Euler
*/
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/uio_driver.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/kobject.h>

#define FREQ 1000
#define UIOMEMSIZE 1024

static long freq = FREQ;
static long my_event_count = 0;

struct uio_info uio_devices_info = 
{
	.name = "uioeuler",
	.version = "0.1",
	.irq = UIO_IRQ_NONE,
};

static int uio_devices_probe(struct device *dev);
static int uio_devices_remove(struct device *dev);

static struct device_driver uio_dummy_driver = 
{
	.name = "uioeuler",
	.bus  = &platform_bus_type,
	.probe= uio_devices_probe,
	.remove = uio_devices_remove,
};

static struct timer_list poll_timer; //generate interruption

static void  uio_devices_timer(unsigned long data)
{
	struct uio_info *info = (struct uio_info *)data;
	unsigned long *addr = (unsigned long *)info->mem[0].addr;
	unsigned long swap = 0;
	if(my_event_count == 0)
	{
		printk("first timer interrupt\n");
		*addr = my_event_count;
	}
	else if(my_event_count == 10)
	{
		printk("timer interrupt happend 10 times\n");
	}
	swap = *addr;
	if(swap != my_event_count)
	{
		printk("counter reset\n");
		my_event_count = swap;
	}
	else
	{
		my_event_count++;
		*addr = my_event_count;
	}
	uio_event_notify(&uio_devices_info); //generate a interrupt
	mod_timer(&poll_timer,jiffies+freq);
}

static int uio_devices_probe(struct device *dev)
{
	printk("androidUAV: %s\n",__func__);
	uio_devices_info.mem[0].addr = (unsigned long)kmalloc(UIOMEMSIZE,GFP_KERNEL);
	if(uio_devices_info.mem[0].addr == 0)
		return -ENOMEM;
	uio_devices_info.mem[0].memtype = UIO_MEM_LOGICAL;
	uio_devices_info.mem[0].size  = UIOMEMSIZE;
	//for the timer interruption
	uio_devices_info.irq_flags = UIO_IRQ_CUSTOM;
	
	if(uio_register_device(dev,&uio_devices_info))
	{
		//error occur
		kfree((void *)uio_devices_info.mem[0].addr);
		return -ENODEV;
	}
	//initilate and add the timer
	init_timer(&poll_timer);
	poll_timer.data = (unsigned long)&uio_devices_info;
	poll_timer.function = uio_devices_timer;
	mod_timer(&poll_timer,jiffies+freq);
	return 0;
}
static int uio_devices_remove(struct device *dev)
{
	uio_unregister_device(&uio_devices_info);
	//
	del_timer_sync(&poll_timer);
	
	return 0;
}

static struct platform_device *uio_dummy_device;

static int __init uio_devices_init(void)
{
	int ret = 0;
	uio_dummy_device = platform_device_register_simple("uioeuler",-1,NULL,0);
	if(!uio_dummy_device)
	{
		printk("androidUAV: erro occur %s",__func__);
		return -1;
	}
	printk("androidUAV:%s platform_device-dev = (%p)\n",__func__,&uio_dummy_device->dev);
	
	ret = driver_register(&uio_dummy_driver);
	return ret;
}
static void __exit uio_devices_exit(void)
{
	platform_device_unregister(uio_dummy_device);
	driver_unregister(&uio_dummy_driver);
}

module_init(uio_devices_init);
module_exit(uio_devices_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Eulerspace Yang");
MODULE_DESCRIPTION("UIO dummy driver for androidUAV Platform");
