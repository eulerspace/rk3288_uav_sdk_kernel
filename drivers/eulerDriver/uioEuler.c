/**
*	This is a simple demo for io driver
*	last modified by
*	12-15-2016 Yang - Euler
*/
#include <linux/module.h>
#include <linux/platform_device.h>
#include <linux/uio_driver.h>
#include <linux/slab.h>
#include <linux/device.h>
#include <linux/kobject.h>
#include <linux/miscdevice.h>
#include <asm/uaccess.h>
#include <linux/err.h>
#include <dt-bindings/gpio/gpio.h>
#include <linux/gpio.h>
#include <linux/of_gpio.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/fb.h>

#define FREQ 1000
#define UIOMEMSIZE 1024
#define MM_SIZE 4096

// global variables
int interruptIO = 263; //GPIO8 A7
char *bufUioMM = NULL;
//generate interrupt use timer
static struct timer_list poll_timer; //generate interruption


static int uioEuler_open(struct inode *inode, struct file *filp);
static ssize_t uioEuler_read(struct file *filp, char __user *ptr, size_t size, loff_t *pos);
static ssize_t uioEuler_write(struct file *filp, char __user *ptr, size_t size, loff_t *pos);
static int uioEuler_mmap(struct file* filp,struct vm_area_struct *vma);
static long uioEuler_ioctl(struct file *filp, int cmd, unsigned long arg);
static int uioEuler_release(struct inode *inode, struct file *filp);
static int uioEuler_probe(struct platform_device *pdev);
static int uioEuler_remove(struct platform_device *pdev);
#ifdef CONFIG_PM_SLEEP
static int uioEuler_suspend(struct device *dev);

static int uioEuler_resume(struct device *dev);
#endif
static long interCount = 0;
//
struct uio_info uio_devices_info = 
{
		.name = "uioEULER",
		.version = "0.1",
		.irq = UIO_IRQ_NONE,
};

static struct of_device_id uioEuler_of_match[] = {
	{ .compatible = "uioEuler" },
	{ }
};
MODULE_DEVICE_TABLE(of,uio_of_match);

static struct file_operations uioEuler_fops = {
	.owner   = THIS_MODULE,
	.open    = uioEuler_open,
	.read    = uioEuler_read,
	.write   = uioEuler_write,
	.mmap	 = uioEuler_mmap,
	.unlocked_ioctl   = uioEuler_ioctl,
	.release = uioEuler_release,
};

static struct miscdevice uioEuler_dev = 
{
    .minor = MISC_DYNAMIC_MINOR,
    .name = "uioEuler",
    .fops = &uioEuler_fops,
};

static const struct dev_pm_ops uioEuler_pm_ops = {
#ifdef CONFIG_PM_SLEEP
	.suspend = uioEuler_suspend,
	.resume = uioEuler_resume,
	.poweroff = uioEuler_suspend,
	.restore = uioEuler_resume,
#endif
};

static struct platform_driver uioEuler_driver = {
	.driver		= {
		.name		= "uioEuler",
		.owner		= THIS_MODULE,
		.pm		= &uioEuler_pm_ops,
		.of_match_table	= of_match_ptr(uioEuler_of_match),
	},
	.probe		= uioEuler_probe,
	.remove		= uioEuler_remove,
};


static int uioEuler_open(struct inode *inode, struct file *filp)
{
	printk("androidUAV : %s\n",__func__);	
	//bufUioMM = (char *)kmalloc(MM_SIZE,GFP_KERNEL);//
	return 0;
}
static int uioEuler_release(struct inode *inode, struct file *filp)
{
	printk("androidUAV : %s\n",__func__);
	/*if(bufUioMM)
	{
		kfree(bufUioMM);
	}*/
	return 0;
}
static ssize_t uioEuler_read(struct file *filp, char __user *ptr, size_t size, loff_t *pos)
{
	
	return 0;
}
static ssize_t uioEuler_write(struct file *filp, char __user *ptr, size_t size, loff_t *pos)
{
	return 0;
}
static int uioEuler_mmap(struct file* filp,struct vm_area_struct *vma)
{
	struct mem_dev *dev = filp->private_data;
	
	//mark virtual mem as IO area
	vma->vm_flags |= VM_IO;
	vma->vm_flags |= (VM_DONTEXPAND | VM_DONTDUMP);
	if(remap_pfn_range(vma,
						vma->vm_start,
						virt_to_phys(uio_devices_info.mem[0].internal_addr)>>PAGE_SHIFT,
						uio_devices_info.mem[0].size,
						vma->vm_page_prot))
	{
		return -EAGAIN;
	}
	return 0;
}
static long uioEuler_ioctl(struct file *filp, int cmd, unsigned long arg)
{
	return 0;
}

static irqreturn_t io_isr(int irq, void *dev_id)
{
	unsigned int pinVal = gpio_get_value(interruptIO);
	unsigned long *addr = (unsigned long *)uio_devices_info.mem[0].addr;
	unsigned long swap = 0;
	if(interCount == 0)
	{
		printk("androidUAV: first interrupt\n");
		*addr = interCount;
	}
	swap = *addr;
	
	if(pinVal)
	{
		//printk("androidUAV:irq high %d %d\n",irq,interCount);
	}
	else
	{
		//printk("androidUAV:irq low %d %d\n",irq,interCount);
	}
	//uio_devices_info
	if(swap != interCount)
	{
		//printk("androidUAV:counter reset\n");
		interCount = swap;
	}
	else
	{
		interCount++;
		*addr = interCount;
		//printk("androidUAV *addr %d\n",(int)*addr);
	}
	uio_event_notify(&uio_devices_info);//generate a interrupt here
	
	return IRQ_HANDLED;
}
static int uioEuler_probe(struct platform_device *pdev)
{
	int ret=-1;
	unsigned long req_flags = IRQF_TRIGGER_FALLING;
	int gpioToIrq;
	ret = misc_register(&uioEuler_dev);
	if (ret < 0){
		printk("uioEuler dev register err!\n");
		return ret;
	}
	gpioToIrq = gpio_to_irq(interruptIO);
    printk("androidUAV: %s\n", __func__);
    uio_devices_info.mem[0].addr = (unsigned long)kmalloc(UIOMEMSIZE,GFP_KERNEL);
	if(uio_devices_info.mem[0].addr == 0)
		return -ENOMEM;
	uio_devices_info.mem[0].memtype = UIO_MEM_LOGICAL;
	uio_devices_info.mem[0].size  = UIOMEMSIZE;
	//for the io interruption
	uio_devices_info.irq_flags = IRQF_DISABLED;
	uio_devices_info.irq = UIO_IRQ_CUSTOM;
	// gpio interrupt test
	if(!gpio_is_valid(interruptIO))
    {
    	printk("androidUAV:invalid iopin %d\n",interruptIO);
    	return -1;
    }
	if(gpio_request(interruptIO,"interIO"))
    {
    	printk("androidUAV:gpio %d request failed!\n",interruptIO);
    	return -1;
    }
    
    printk("androidUAV:gpioToIrq %d request!\n",gpioToIrq);
    if(gpioToIrq < 0)
    {
    	printk("androidUAV:%d gpioToIrq failed!\n",gpioToIrq);
    	return -1;
    }
    gpio_direction_input(interruptIO);

    ret = request_irq(gpioToIrq,io_isr,IRQF_TRIGGER_RISING,"GPIOIrqEuler",NULL);//IRQF_TRIGGER_FALLING|
    printk("androidUAV:%d ret!\n",ret);
    
    if(uio_register_device(&(pdev->dev),&uio_devices_info))
	{
		printk("androidUAV: error occur %s\n", __func__);
		//error occur
		return -ENODEV;
	}
	
	return 0;
}
static int uioEuler_remove(struct platform_device *pdev)
{
	return 0;
}
#ifdef CONFIG_PM_SLEEP
static int uioEuler_suspend(struct device *dev)
{
	return 0;
}

static int uioEuler_resume(struct device *dev)
{
	return 0;
}
#endif


module_platform_driver(uioEuler_driver);

MODULE_DESCRIPTION("uioEuler Driver");
MODULE_LICENSE("GPL");
MODULE_ALIAS("platform:uioEuler");
